import pkg from './package.json';

export default [
    {
        input: 'src/main.js',
        output: {
            file:  pkg.browser,
            format: 'umd',
            name: 'PluginValidator'
        }
    },

    // OUTPUT TO TEST
    {
        input: 'src/main.js',
        output: {
            file: pkg.test,
            format: 'umd',
            name: 'PluginValidator'
        }
    }

]