(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = global || self, global.PluginValidator = factory());
}(this, (function () { 'use strict';

    class Helpers
    {

        static onlyNumbers(value)
        {
            return /[0-9]/.test(value);
        }

        static sizeLength(value, size)
        {
            if (value.length != size) return false;

            return true;
        }

        static removeMask(value)
        {
            let regex = /[!-/ ]+/g;

            return value.replace(regex, "");
        }

        static isNotEmpty(value)
        {

            if (value === '0') return false;

            if (typeof value !== 'undefined' && value !== '' && value !== null)
                return true;

            return false;  
        }

        static isValidatedNumbers8(value)
        {
            let reg = /([0-9])\1{7}/;
            if (reg.test(value)) return false;

            return true;
        }

        static isValidatedNumbers10(value)
        {
            let reg = /([0-9])\1{9}/;
            if (reg.test(value)) return false;

            return true;
        }


        static isValidatedNumbers11(value)
        {
            let reg = /([0-9])\1{10}/;
            if (reg.test(value)) return false;

            return true;
        }

        static isValidatedNumbers14(value)
        {
            let reg = /([0-9])\1{13}/;
            if (reg.test(value)) return false;

            return true;
        }

        static isEmailValid(value)
        {   
            return /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value);
        }
    }

    class CpfService 
    {
        static isCpfValid(value)
        {
            let digitOne = this.getDigitOne(value);   
            let digitTwo = this.getDigitTwo(value, digitOne);
            let partValue = value.substr(0, 9);
            let cpf = partValue.concat(digitOne, digitTwo);

            if (cpf == value) return true;
            
            return false;
        }

        static getDigitOne(value)
        {   
            let sumFirstDigit = 0;
            let multipliedBy = 11;

            for (let i = 0; i < value.substr(0, 9).length; i++) {
                sumFirstDigit += ((value.substr(i, 1)) * Number(multipliedBy - (i + 1)));            
            } 
            
            let rest = sumFirstDigit % 11;
            if (rest < 2) return 0;  

            return Number(11 - rest);     
        }

        static getDigitTwo(value, digitOne)
        {
            let sumSecondDigit = 0;
            let multipliedBy = 12;

            let substr = value.substr(0, 9);
            let newValue = substr.concat(digitOne);        

            for (let i = 0; i < newValue.length; i++)  {
                sumSecondDigit += ((newValue.substr(i, 1)) * Number(multipliedBy - (i + 1)));    
            }
                
            let rest = sumSecondDigit % 11;     
            if (rest < 2) return 0;  

            return Number(11 - rest);     
        }
    }

    class DateService
    {
        static isDateValid(value)
        {     
            if (! Helpers.isNotEmpty(value)) return false;    
            
            let date = null;
            if (value.indexOf('-') !== -1) {            
                date = this.isDateValidInputDate(value);        
                return this.isDate(date);
            }
            
            date = this.isDateValidInputText(value);
            return this.isDate(date);
        }

        static isDateValidInputDate(value)
        {
            return new Date(value);
        }

        static isDateValidInputText(value)
        {    
           let date = value.split('/').map((item) => item);
        
           if (date[0] == '30' && date[1] == '02') return false;
           if (date[0] == '31' && date[1] == '02') return false;

           return new Date(date.reverse());
        }

        static isDate(value) 
        {
            if(value instanceof Date) { 
                let date = new Date(value);                                
                if (1900 < date.getFullYear()) return true;
            }

            return false;
        }
    }

    class Rules 
    {
        validate(args)
        {
            let self = this;

            return args.rules.every(function(rule) {

                if (typeof self[rule] !== 'function')
                    throw ('Nome de função de validação inválida/inexistente, valor infomado = ' + rule);  

                return self[rule](args.value);
            });
        }

        isString(value)
        {
            if(!this.isNotEmpty(value)) return false;        
            if (typeof value === 'string') return true;

            return false;
        }

        isInt(value)
        {
            if(!this.isNotEmpty(value)) return false;

            let result = Number(value);
            if (!isNaN(result) && typeof result == 'number') return true;

            return false;        
        }

        isNotEmpty(value)
        {
            return Helpers.isNotEmpty(value);
        }

        isCpf(value)
        {
            if(!this.isNotEmpty(value)) return false;

            let withoutMask = Helpers.removeMask(value);
            if(!Helpers.onlyNumbers(withoutMask)) return false;
            if(!Helpers.sizeLength(withoutMask, 11)) return false;
            if(!Helpers.isValidatedNumbers10(withoutMask)) return false;

            return CpfService.isCpfValid(withoutMask);        
        }

        isCnpj(value) 
        {
            if(!this.isNotEmpty(value)) return false;

            let withoutMask = Helpers.removeMask(value);        
            if(!Helpers.onlyNumbers(withoutMask)) return false;
            if(!Helpers.sizeLength(withoutMask, 14)) return false;  
            if(!Helpers.isValidatedNumbers14(withoutMask)) return false;     

            return true;
        }

        isCpfOrCnpj(value) 
        {
            if (!this.isCpf(value) && !this.isCnpj(value)) return false;
            
            return true;
        }


        isZipCode(value)
        {
            if(!this.isNotEmpty(value)) return false;
            let withoutMask = Helpers.removeMask(value);
            if(!Helpers.onlyNumbers(withoutMask)) return false;
            if(!Helpers.sizeLength(withoutMask, 8)) return false;      
            if(!Helpers.isValidatedNumbers8(withoutMask)) return false;  


            return true;
        }

        isPhone(value)
        {
            if(!this.isNotEmpty(value)) return false;

            let withoutMask = Helpers.removeMask(value);
            if(!Helpers.onlyNumbers(withoutMask)) return false;
            if(!Helpers.sizeLength(withoutMask, 10)) return false;    
            if(!Helpers.isValidatedNumbers10(withoutMask)) return false;

            return true;
        }

        isCellphone(value)
        {
            if(!this.isNotEmpty(value)) return false;

            let withoutMask = Helpers.removeMask(value);
            if(!Helpers.onlyNumbers(withoutMask)) return false;
            if(!Helpers.sizeLength(withoutMask, 11)) return false;    
            if(!Helpers.isValidatedNumbers11(withoutMask)) return false;

            return true;
        }

        isDate(value)
        {
            return DateService.isDateValid(value);
        }

        isOfAge(value)
        {
        
            if (!this.isDate(value)) return false;
            
            let dateNow = new Date().getFullYear();
            let dateParam = null;

            if (value.indexOf('-') !== -1) dateParam = DateService.isDateValidInputDate(value).getFullYear();                  
            else dateParam = DateService.isDateValidInputText(value).getFullYear();
            
            if (dateParam > dateNow) return false;

            if ((dateNow - dateParam) < 18) return false;
            return true
        }

        isFullName(value) 
        {
            if (! this.isNotEmpty(value)) return false;
            
            let split = value.trim().split(' ');
            if (split.length < 2) return false;

            return true;
        }

        isEmail(value) 
        {
            if (! this.isNotEmpty(value)) return false;
            if (! Helpers.isEmailValid(value)) return false;

            return true;        
        }
    }

    class Validator
    {
        constructor(args)
        {
           this.args = args;       
        }

        start()
        {
            this.validateArgs();
            this.setProperties();

            return this.validatorRules();
        }

        validateArgs()
        {
            if (!this.args) throw ('Argumentos inválidos ou inexistentes!');
            if (this.args.validators === undefined) throw ('Propriedade validators inválida ou inexistente!')
        }

        setProperties()
        {
            for (let item of this.args.validators) {        

                let property = {
                    'value': item.element.value,
                    'rules': item.rules,
                    'response': item.response 
                };

                this[item.element.name] = property;
            }
        }

        validatorRules()
        {
            let properties = Object.assign(this);      
            delete properties.args;
           
            let rules = new Rules();
            let result = [];
            let isValid = true;

            Object.keys(properties).map((key) => {

                let validate = rules.validate(properties[key]);

                if (!validate) isValid = false;
                result.push({ name: key, valid: validate, response: properties[key].response });
            });

            result.isValid = isValid;
           
            return result;
        }

    }

    function main(args)
    {   
        return new Validator(args).start();  
    }

    return main;

})));
