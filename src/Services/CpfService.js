import Helpers from '../Helpers/Helpers';
class CpfService 
{
    static isCpfValid(value)
    {
        let digitOne = this.getDigitOne(value);   
        let digitTwo = this.getDigitTwo(value, digitOne);
        let partValue = value.substr(0, 9);
        let cpf = partValue.concat(digitOne, digitTwo);

        if (cpf == value) return true;
        
        return false;
    }

    static getDigitOne(value)
    {   
        let sumFirstDigit = 0;
        let multipliedBy = 11;

        for (let i = 0; i < value.substr(0, 9).length; i++) {
            sumFirstDigit += ((value.substr(i, 1)) * Number(multipliedBy - (i + 1)));            
        } 
        
        let rest = sumFirstDigit % 11;
        if (rest < 2) return 0;  

        return Number(11 - rest);     
    }

    static getDigitTwo(value, digitOne)
    {
        let sumSecondDigit = 0;
        let multipliedBy = 12;

        let substr = value.substr(0, 9);
        let newValue = substr.concat(digitOne);        

        for (let i = 0; i < newValue.length; i++)  {
            sumSecondDigit += ((newValue.substr(i, 1)) * Number(multipliedBy - (i + 1)));    
        }
            
        let rest = sumSecondDigit % 11;     
        if (rest < 2) return 0;  

        return Number(11 - rest);     
    }
}

export default CpfService;