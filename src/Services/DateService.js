import Helpers from '../Helpers/Helpers';

class DateService
{
    static isDateValid(value)
    {     
        if (! Helpers.isNotEmpty(value)) return false;    
        
        let date = null;
        if (value.indexOf('-') !== -1) {            
            date = this.isDateValidInputDate(value);        
            return this.isDate(date);
        }
        
        date = this.isDateValidInputText(value);
        return this.isDate(date);
    }

    static isDateValidInputDate(value)
    {
        return new Date(value);
    }

    static isDateValidInputText(value)
    {    
       let date = value.split('/').map((item) => item);
    
       if (date[0] == '30' && date[1] == '02') return false;
       if (date[0] == '31' && date[1] == '02') return false;

       return new Date(date.reverse());
    }

    static isDate(value) 
    {
        if(value instanceof Date) { 
            let date = new Date(value);                                
            if (1900 < date.getFullYear()) return true;
        }

        return false;
    }
}

export default DateService;