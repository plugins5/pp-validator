
import Validator from './validator'

export default function(args)
{   
    return new Validator(args).start();  
}
