import Rules from './Rules/Rules';

class Validator
{
    constructor(args)
    {
       this.args = args;       
    }

    start()
    {
        this.validateArgs();
        this.setProperties();

        return this.validatorRules();
    }

    validateArgs()
    {
        if (!this.args) throw ('Argumentos inválidos ou inexistentes!');
        if (this.args.validators === undefined) throw ('Propriedade validators inválida ou inexistente!')
    }

    setProperties()
    {
        for (let item of this.args.validators) {        

            let property = {
                'value': item.element.value,
                'rules': item.rules,
                'response': item.response 
            }

            this[item.element.name] = property
        }
    }

    validatorRules()
    {
        let properties = Object.assign(this);      
        delete properties.args;
       
        let rules = new Rules();
        let result = [];
        let isValid = true;

        Object.keys(properties).map((key) => {

            let validate = rules.validate(properties[key]);

            if (!validate) isValid = false;
            result.push({ name: key, valid: validate, response: properties[key].response });
        })

        result.isValid = isValid;
       
        return result;
    }

}

export default Validator