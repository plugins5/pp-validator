import Helpers from '../Helpers/Helpers';
import Cpf from '../Services/CpfService';
import DateService from '../Services/DateService';

class Rules 
{
    validate(args)
    {
        let self = this;

        return args.rules.every(function(rule) {

            if (typeof self[rule] !== 'function')
                throw ('Nome de função de validação inválida/inexistente, valor infomado = ' + rule);  

            return self[rule](args.value);
        });
    }

    isString(value)
    {
        if(!this.isNotEmpty(value)) return false;        
        if (typeof value === 'string') return true;

        return false;
    }

    isInt(value)
    {
        if(!this.isNotEmpty(value)) return false;

        let result = Number(value);
        if (!isNaN(result) && typeof result == 'number') return true;

        return false;        
    }

    isNotEmpty(value)
    {
        return Helpers.isNotEmpty(value);
    }

    isCpf(value)
    {
        if(!this.isNotEmpty(value)) return false;

        let withoutMask = Helpers.removeMask(value);
        if(!Helpers.onlyNumbers(withoutMask)) return false;
        if(!Helpers.sizeLength(withoutMask, 11)) return false;
        if(!Helpers.isValidatedNumbers10(withoutMask)) return false;

        return Cpf.isCpfValid(withoutMask);        
    }

    isCnpj(value) 
    {
        if(!this.isNotEmpty(value)) return false;

        let withoutMask = Helpers.removeMask(value);        
        if(!Helpers.onlyNumbers(withoutMask)) return false;
        if(!Helpers.sizeLength(withoutMask, 14)) return false;  
        if(!Helpers.isValidatedNumbers14(withoutMask)) return false;     

        return true;
    }

    isCpfOrCnpj(value) 
    {
        if (!this.isCpf(value) && !this.isCnpj(value)) return false;
        
        return true;
    }


    isZipCode(value)
    {
        if(!this.isNotEmpty(value)) return false;
        let withoutMask = Helpers.removeMask(value);
        if(!Helpers.onlyNumbers(withoutMask)) return false;
        if(!Helpers.sizeLength(withoutMask, 8)) return false;      
        if(!Helpers.isValidatedNumbers8(withoutMask)) return false;  


        return true;
    }

    isPhone(value)
    {
        if(!this.isNotEmpty(value)) return false;

        let withoutMask = Helpers.removeMask(value);
        if(!Helpers.onlyNumbers(withoutMask)) return false;
        if(!Helpers.sizeLength(withoutMask, 10)) return false;    
        if(!Helpers.isValidatedNumbers10(withoutMask)) return false;

        return true;
    }

    isCellphone(value)
    {
        if(!this.isNotEmpty(value)) return false;

        let withoutMask = Helpers.removeMask(value);
        if(!Helpers.onlyNumbers(withoutMask)) return false;
        if(!Helpers.sizeLength(withoutMask, 11)) return false;    
        if(!Helpers.isValidatedNumbers11(withoutMask)) return false;

        return true;
    }

    isDate(value)
    {
        return DateService.isDateValid(value);
    }

    isOfAge(value)
    {
    
        if (!this.isDate(value)) return false;
        
        let dateNow = new Date().getFullYear();
        let dateParam = null;

        if (value.indexOf('-') !== -1) dateParam = DateService.isDateValidInputDate(value).getFullYear();                  
        else dateParam = DateService.isDateValidInputText(value).getFullYear();
        
        if (dateParam > dateNow) return false;

        if ((dateNow - dateParam) < 18) return false;
        return true
    }

    isFullName(value) 
    {
        if (! this.isNotEmpty(value)) return false;
        
        let split = value.trim().split(' ');
        if (split.length < 2) return false;

        return true;
    }

    isEmail(value) 
    {
        if (! this.isNotEmpty(value)) return false;
        if (! Helpers.isEmailValid(value)) return false;

        return true;        
    }
}

export default Rules;
