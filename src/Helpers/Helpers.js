class Helpers
{

    static onlyNumbers(value)
    {
        return /[0-9]/.test(value);
    }

    static sizeLength(value, size)
    {
        if (value.length != size) return false;

        return true;
    }

    static removeMask(value)
    {
        let regex = /[!-/ ]+/g;

        return value.replace(regex, "");
    }

    static isNotEmpty(value)
    {

        if (value === '0') return false;

        if (typeof value !== 'undefined' && value !== '' && value !== null)
            return true;

        return false;  
    }

    static isValidatedNumbers8(value)
    {
        let reg = /([0-9])\1{7}/;
        if (reg.test(value)) return false;

        return true;
    }

    static isValidatedNumbers10(value)
    {
        let reg = /([0-9])\1{9}/;
        if (reg.test(value)) return false;

        return true;
    }


    static isValidatedNumbers11(value)
    {
        let reg = /([0-9])\1{10}/;
        if (reg.test(value)) return false;

        return true;
    }

    static isValidatedNumbers14(value)
    {
        let reg = /([0-9])\1{13}/;
        if (reg.test(value)) return false;

        return true;
    }

    static isEmailValid(value)
    {   
        return /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value);
    }
}

export default Helpers;